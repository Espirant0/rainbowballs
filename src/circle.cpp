#include <circle.hpp>
#include <iostream>


namespace saw {
	Circle::Circle(int x, int y, float r,  float velocity)
	{
		m_x = x;
		m_y = y;
		m_r = r;
		m_velocity = velocity;
	}
	 bool Circle::Setup()
	{
		if (!m_texture1.loadFromFile("img/rainbow.png"))
		{
			std::cout << "ERROR when loading rainbow.png" << std::endl;
			return false;
		}

		m_shape = new sf::Sprite();
		m_shape->setTexture(m_texture1);
		m_shape->setOrigin(m_r, m_r);
		m_shape->setPosition(m_x, m_y);
		return true;
	}


	
	Circle::~Circle()
	{
		if (m_shape != nullptr)
			delete m_shape;
	}

	sf::Sprite* Circle::GetCircle() { return m_shape; }

	void Circle::Move()
	{
		m_x += m_velocity;
		m_shape->setPosition(m_x, m_y);
	}
	void Circle::MoveBack()
	{
		m_x -= m_velocity;
		m_shape->setPosition(m_x, m_y);
	}
	

	int Circle::GetX() { return m_x; }
	int Circle::GetY() { return m_y; }
	float Circle::GetR() { return m_r; }
}