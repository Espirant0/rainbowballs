#include <ball.hpp>
#include <iostream>

namespace saw {
		Ball::Ball(int x0, int y0, float r, float angle, float v0) {
			m_x0 = x0;
			m_y0 = y0;
			m_r = r;
			m_v0 = v0;
			m_angle = (2 * acos(-1) - angle);
		}
		bool Ball::SetUp() {
			if (!m_texture.loadFromFile("img/ball.png"))
			{
				std::cout << "ERROR! PICTURE NOT FOUND!" << std::endl;
				return false;
			}

			m_ball = new sf::Sprite();
			m_ball->setTexture(m_texture);
			m_ball->setOrigin(m_r, m_r);
			m_ball->setPosition(m_x, m_y);
			m_ball->setScale(1.4, 1.4);
			return true;
		}
		Ball::~Ball() {
			if (m_ball != nullptr)
				delete m_ball;
		}


		void Ball::Move(float t) {
			m_x = m_x0 + m_v0 * cos(m_angle) * t;
			m_y = m_y0 + m_v0 * sin(m_angle) * t;
			m_ball->setPosition(m_x, m_y);
		}
		sf::Sprite* Ball::GetBall() {
			return m_ball;
		}

		int Ball::GetX() { return m_x; }
		int Ball::GetY() { return m_y; }
		float Ball::GetR() { return m_r; }


}