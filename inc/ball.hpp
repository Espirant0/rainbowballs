#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>

namespace saw {

	class Ball {
	public:
		Ball(int x0, int y0, float r, float angle, float v0);
		bool SetUp();
		~Ball();
		void Move(float t);
		sf::Sprite* GetBall();
		int GetX();
		int GetY();
		float GetR();
		
	private:
		int m_x, m_y, m_x0, m_y0;
		float m_r;
		float m_angle;
		float m_v0;
		sf::Texture m_texture;
		sf::Sprite* m_ball = nullptr;
	};


	
}