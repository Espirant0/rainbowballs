#pragma once
#include <SFML/Graphics.hpp>


namespace saw {
	class Circle {
	public:
		Circle(int x, int y, float r, float velocity);
		~Circle();
		bool Setup();
		sf::Sprite* GetCircle();
		void Move();
		void MoveBack();
		int GetX();
		int GetY();
		float GetR();
		
	private:
		int m_x, m_y;
		float m_r, m_velocity;
		sf::Texture m_texture1;
		sf::Sprite* m_shape = nullptr;

	};
}