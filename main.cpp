#include <SFML/Graphics.hpp>
#include<iostream>
#include<locale>
#include<windows.h>
#include<thread>
#include<chrono>
#include <circle.hpp>
#include <ball.hpp>
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

using namespace std::chrono_literals;
using namespace saw;



int main()
{
    srand(time(0));
    const int width = 800;
    const int height = 600;
    int flag = 0;
    float t = 0;
    float sx = 398;
    float sy = 577;
    int score = 0;
    int attempts = 6;
    //  Создание окна(раземер, заголовок)
    sf::RenderWindow window(sf::VideoMode(800, 600), "Rainbow balls"); 
    //Создание шарика
    Ball* ball= nullptr;

    // Подгрузка фонового изображения
    sf::Texture texture_back;
    if (!texture_back.loadFromFile("img/back.jpg"))
    {
        std::cout << "ERROR when loading back.jpg" << std::endl;
        return false;
    }
    sf::Sprite back;
    back.setTexture(texture_back);

    //Подгрузка победного фона
    sf::Texture texture_back_win;
    if (!texture_back_win.loadFromFile("img/back1.png"))
    {
        std::cout << "ERROR when loading back1.png" << std::endl;
        return false;
    }
    sf::Sprite back_win;
    back_win.setTexture(texture_back_win);


    // Добавление иконки
    sf::Image icon;
    if (!icon.loadFromFile("img/rainbow.png"))
    {
        return -1;
    }
    window.setIcon(48, 48, icon.getPixelsPtr());

    //Подгрузка шрифта
    sf::Font font;
    if (!font.loadFromFile("fonts/BUBBCB.ttf"))
    {
        std::cout << "ERROR: font was not loaded." << std::endl;
        return -1;
    }
    //Отрисовка счёта
    sf::Text text;
    text.setFont(font);
    text.setString("Hello world");
    text.setCharacterSize(24);
    text.setFillColor(sf::Color::White);
    text.setPosition(650,565);

    //Отрисовка попыток
    sf::Text text4;
    text4.setFont(font);
    text4.setString("Hello world");
    text4.setCharacterSize(24);
    text4.setFillColor(sf::Color::White);
    text4.setPosition(605, 530);
   
    //Отрисовка победной надписи
    sf::Text text1;
    text1.setFont(font);
    text1.setString("Hello world");
    text1.setCharacterSize(40);
    text1.setFillColor(sf::Color::Black);
    text1.setPosition(300, 250);
    text1.setString(std::string("You won! "));

    //Отрисовка поражения
    sf::Text text2;
    text2.setFont(font);
    text2.setString("Hello world");
    text2.setCharacterSize(40);
    text2.setFillColor(sf::Color::Black);
    text2.setPosition(300, 250);
    text2.setString(std::string("You lose! "));

    //Инициализация фигур
    const int N = 10;
    std::vector<Circle*> circles;
    for (int i = 0; i < N; i++)
    {
        circles.push_back(new Circle(0, 50, 20, rand() % 17 + 1));
    }
    

    while (window.isOpen())
    {
        // Переменная для хранения события
        sf::Event event;
        // Цикл по всем событиям
        while (window.pollEvent(event))
        {
            //Обработка события 
            if (event.type == sf::Event::Closed)
                window.close();
        }

        //Задание скорости и угла полёта шарика
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            while (sf::Mouse::isButtonPressed(sf::Mouse::Left));
            sf::Vector2i mp = sf::Mouse::getPosition(window);
            float d = sqrt((mp.x - sx) * (mp.x - sx) + (mp.y - sy) * (mp.y - sy));
            float ay = mp.y - sy;
            float ax = mp.x - sx;
            attempts -= 1;
            float angle = acos(ax / sqrt(ax * ax + ay * ay));
            if (ball != nullptr)
                delete ball;
            ball = new Ball(sx, sy, 20, angle, d);
            //Проверка загрузки спрайта для шарика
            if (!ball->SetUp())
            {
                delete ball;
                window.close();
                return -1;
            }
           
            t = 0;
        }
        //Проверка загрузки спрайта для кругов
        for (const auto& circle : circles)
            if (!circle->Setup())
                return -1;
        //Задание движения кругов
        for (auto& circle : circles) {
            if (circle->GetX() > width)
                flag = 1;
            if (flag == 1)
                circle->MoveBack();
            if (circle->GetX() < 0)
                flag = 0;
            if (flag == 0)
                circle->Move();
        }

        //Проверка соударения шарика с кругами
        if (ball != nullptr) {
            ball->Move(t);
           
            for (int i = 0; i < circles.size(); i++)
            {
                int X = ball->GetX();
                int Y = ball->GetY();
                float R = ball->GetR();

                int x = circles[i]->GetX();
                int y = circles[i]->GetY();
                float r = circles[i]->GetR();
                float d = sqrt((X - x) * (X - x) + (Y - y) * (Y - y));

                if (R + r >= d)
                {
                    delete circles[i];
                    circles.erase(circles.begin() + i);
                    i--;
                    score++;
                }    
            }
        }
        window.clear();
        window.draw(back);
        //Отрисовка счёта и попыток
        text.setString(std::string("Score ") + std::to_string(score));
        text4.setString(std::string("Attempts ") + std::to_string(attempts+1));
        //Победа
        if (score == N) {
            window.draw(back_win);
            window.draw(text1);
            text4.setScale(0, 0);
        }
        else
        //Поражение
        if (attempts < -1) {
            window.draw(text2);
            text4.setString(std::string("Attempts 0 "));
            for (int i = 0; i < circles.size(); i++) {
                delete circles[i];
                circles.erase(circles.begin() + i);

            }
        }

        window.draw(text);
        window.draw(text4);
        
        if (ball != nullptr)
            window.draw(*ball->GetBall());
        
        for (const auto& circle : circles)
            window.draw(*circle->GetCircle());
        
        window.display();
        std::this_thread::sleep_for(40ms);
        t += 0.04;
    }

    //Очистка памяти
    if (ball != nullptr)
        delete ball;
    for (const auto& circle : circles)
        delete circle;
    circles.clear();
    return 0;
}